# Asymmetrie umkehren
---

Wir erinnern uns an das Prinzip der reinen asymmetrische Verschlüsselung:

![](../../3-publickey/4-asymm-encryption/images/asymm-encryption.svg)

## Gedankenexperiment

Was würde geschehen, wenn wir den Vorgang ändern?

Bei der asymmetrischen Verschlüsselung gibt es zwei Schlüssel, den öffentlichen und den privaten. Normalerweise verwendet Alice zur Verschlüsselung den öffentlichen Schlüssel von Bob. Bob wieder entschlüsselt den übermittelten Geheimtext mit seinem privaten Schlüssel.

::: info
Was geschieht, wenn Alice den Klartext mit **ihrem eigenen privaten** Schlüssel «verschlüsselt»?
:::

Dies lässt sich mit Schloss und Schlüssel leider nicht mehr sinnvoll und verständlich darstellen. Wir stellen uns die asymmetrische Ver-/Entschlüsselung einfach als reine mathematische Angelegenheit vor:

Der Verschlüsselungsfunktion ($E$) wird normalerweise der Klartext ($p$) und der öffentliche Schlüssel ($e$) übergeben, dabei entsteht der Geheimtext ($c$):

$$c = E(p, e)$$

Die Entschlüsselungsfunktion ($D$) berechnet aus dem Geheimtext ($c$) und dem privaten Schlüssel ($d$) wieder den Klartext ($p$):

$$p = D(c, d)$$

::: exercise Aufgabe Gedankenexperiment
1. Was erhalten wir, wenn wir der Verschlüsselungsfunktion anstelle des öffentlichen Schlüssels von Bob den **privaten Schlüssel von Alice** übergeben?

   $$? = E(p, d)$$

2. Wie lässt sich dieses «Produkt» wieder in einen Klartext verwandeln?

   $$p = ?(?, ?)$$
***
1. Es entsteht ebenso ein «spezieller Geheimtext», also etwas, das nicht direkt verstanden werden kann, wir bezeichnen dieses Produkt mit dem Buchstaben $s$.

   $$s = E(p, d)$$

2. Da dieser spezielle «spezielle Geheimtext» $s$ mit dem **privaten** Schlüssel von Alice «verschlüsselt» wurde, kann er nur mit dem zugehörigen **öffentlichen** Schlüssel von Alice wieder in Klartext umgewandelt werden.

   $$p = D(s, e)$$
:::

::: exercise Aufgabe Bedeutung des Gedankenexperimentes
Was bedeutet es, wenn jemand einen Text mit seinem eigenen **privaten Schlüssel «verschlüsselt»**, so dass der Geheimtext nur mit dem zugehörigen **öffentlichen Schlüssel «entschlüsselt»** werden kann?

Was sagt die Tatsache, dass man einen solchen Geheimtext mit einem fremden (nicht dem eigenen) öffentlichen Schlüssel entschlüsseln kann, aus?
:::

## Digitale Signatur

Wir haben soeben das Prinzip der digitalen Signatur entdeckt.

![Digitale Signatur](./images/asymm-signature.svg)

::: info Digitale Signatur
Wenn Bob die Nachricht mit dem **öffentlichen Schlüssel von Alice** entschlüsseln kann, dann muss diese Nachricht vorher mit dem **privaten Schlüssel von Alice** verschlüsselt worden sein. Somit kann sie von niemandem anders stammen als von Alice. D.h. diese Nachricht ist quasi digital signiert.
:::

::: exercise Aufgabe mehrere Leute
Überlege dir, wie ein signiertes Dokument von mehr als zwei Personen angeschaut und überprüft wird. Welche Schlüssel werden verwendet?
***
Es wird nur das Schlüsselpaar von Alice verwendet:

![Überprüfung der Signatur durch mehrere Personen](./images/hash-asymm-signature.svg)
:::

## Nachteile

Die bisher kennengelernte Art der digitalen Signatur ist nicht optimal, denn es gibt mehrere Probleme:

- Wegen der «Signatur» ist der **Klartext nicht mehr lesbar** (er ist ja nun speziell «verschlüsselt»). Dies ist umständlich, denn wenn du ein Dokument auf Papier unterschreibst, bleibt dieses trotzdem lesbar.
- **Asymmetrische Verfahren sind langsam**, d.h. wenn wir ein grosses Dokument (z.B. ein Video) signieren, dauert dieser Vorgang unnötig lange – ebenso das Überprüfen der Signatur (also das Entschlüsseln).
