# Zertifizierungsstellen
---

## Übertragung öffentlicher Schlüssel

Öffentliche Schlüssel enthalten keine geheime Information, können also problemlos über einen offenen Kanal übertragen werden. Oder?

![Angriff auf den öffentlichen Schlüssel](./images/public-key-attack-1.svg)

::: exercise Aufgabe Angriff auf den öffentlichen Schlüssel
Alice schickt ihren öffentlichen Schlüssel (also ihr Vorhängeschloss) per Post an Bob. Überlege dir, was der bösartige Mallory anstellen könnte, um Alice und Bob zu überlisten.
***
Da der öffentliche Schlüssel nur aus Zahlen besteht, ist es für Bob nicht erkennbar, ob er den richtigen Schlüssel erhalten hat.

![Man-in-the-middle-Attacke](./images/public-key-attack-2.svg)

:::

## Schutz gegen Man-in-the-Middle-Attacken

Wie können wir uns vor Man-in-the-Middle-Attacken schützen? Kann uns Trent helfen?

![Die Rolle von Trent](./images/certificates-task.svg)

::: exercise Aufgabe Zertifizierungsstellen
Wie könnte Trent dafür sorgen, dass die öffentlichen Schlüssel besser überprüfbar sind?
***
Trent ist bekannt und akzeptiert als Zertifizierungsstelle, alle kennen seinen öffentlichen Schlüssel. Wenn Trent die Kunden gut überprüft und deren Schlüssel signiert, kann Bob sicher sein, den richtigen Schlüssel zu verwenden. Er akzeptiert folglich keine unsignierten Schlüssel mehr.

![Schutz vor Man-in-the-Middle-Attacken](./images/certificates.svg)
:::
