# WhatsApp-Verschlüsselung
---

Gemäss WhatsApp sind sämtliche WhatsApp-Nachrichten Ende-zu-Ende verschlüsselt.[^1]

::: exercise Aufgabe WhatsApp-Verschlüsselung
1. Was meint WhatsApp mit «Ende-zu-Ende-Verschlüsselung»?
2. Diskutiert zu zweit, wie die WhatsApp-Verschlüsselung funktionieren könnte.
:::

::: exercise Aufgabe Sicherheitsnummer
WhatsApp bietet die Möglichkeit, eine Sicherheitsnummer anzuzeigen und zu überprüfen.

Ende-zu-Ende-Verschlüsselung eines Chats verifizieren:
- Öffnet euren gemeinsamen Chat.
- Tippt auf den Namen des Kontakts, um den Kontaktinfo-Bildschirm zu öffnen.
- Tippt auf _Verschlüsselung_, um den QR-Code und die 60-stellige Sicherheitsnummer anzuzeigen.
- Ihr könnt den angezeigten QR-Code gegenseitig scannen, um nicht die 60-stellige Zahl kontrollieren zu müssen.

Probiert es zu zweit aus und diskutiert, wie diese Nummer zustande kommt und was sie bedeutet.
:::

[^1]: Quelle: [WhatsApp](https://faq.whatsapp.com/general/security-and-privacy/end-to-end-encryption/?lang=de)
