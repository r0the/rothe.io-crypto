# Browserzertifikate
---

Während früher sämtliche Informationen im Internet unverschlüsselt übertragen worden sind, werden die meisten Verbindungen heutzutage mit kryptographischen Mitteln geschützt.

Insbesondere nutzen momentan (März 2021) gemäss W3Techs[^1] über 70% aller Webseiten HTTPS. HTTPS ist eine Erweiterung des **HyperText Transfer Protocol**s, der Buchstabe «S» steht für **Secure**.

Damit wird der Datenverkehr in beiden Richtungen verschlüsselt:

- alle Daten, die vom Webserver zum Browser übertragen werden (also der Inhalt der Seite wie z.B. Text, Bilder, Videos, heruntergeladene Dateien, ...)
- alle Daten, die vom Browser zu Webserver übertragen werden (also ausgefüllte Formulare wie z.B. Benutzernamen und Passwörter, aber auch alle Texte, die wir eingeben sowie sämtliche hochgeladenen Dateien, ...)

## Symmetrische und asymmetrische Verschlüsselung

Die Daten werden symmetrisch verschlüsselt, der Sitzungsschlüssel (wie bereits besprochen) asymmetrisch.

Da die Browser nicht jeden einzelnen öffentlichen Schlüssel der Webseiten (dies sind gemäss internet live stats hunderte Millionen[^2]) kennen können, vertrauen sie ausgewählten Zertifizierungsstellen. Der öffentliche Schlüssel einer Webseite wird von einer Zertifizierungsstelle digital signiert. Das so entstandene Zertifikat wird beim Verbindungsaufbau übertragen und kann vom Browser überprüft werden.

## Webseitenzertifikate

![Das Schloss vor der URL zeigt die Verwendung von HTTPS (und Webseitenzertifikaten) an](./images/website-certificate.png)

Klickst du auf das Schloss, kannst du «Weitere Informationen» zur Verbindung anzeigen:

![Informationen zur Webseite](./images/website-certificate-2.png)

::: exercise Aufgabe Webseitenzertifikat
Was findest du alles über die Zertifikate der Webseite des Gymnasiums Kirchenfeld heraus?
:::

::: exercise Aufgabe kryptographische Details
Im Screenshot oben siehst du die technische Angabe **TLS_AES_256_GCM_SHA384, 256-Bit Schlüssel, TLS 1.3**. Findest du heraus, was die einzelnen Teile aussagen?
:::

::: exercise Aufgabe Zertifizierungsstellen
Findest du die im Browser gespeicherten «vertrauenswürdigen Zertifizierungsstellen»?
:::

::: exercise Aufgabe Zertifikatswarnungen
Besuche die Webseite [BadSSL](https://badssl.com/) und teste mit verschiedenen Browser verschiedene Konfigurationen. So lernst du mögliche Zertifikatswarnungen kennen.
:::

## Achtung vor falscher Sicherheit

Die Tatsache, dass bei einer bestimmten Webseite das Schloss angezeigt wird, heisst noch lange nicht, dass die Webseite **sicher** ist. Denn das Wort **sicher** hat mehrere Bedeutungen.

Was genau sagt das Schloss also aus? Worauf können wir vertrauen? Was hingegen kann trotzdem noch schiefgehen?

Was bedeutet **sicher** in diesem Zusammenhang?

1. Die Verbindung ist vertraulich – niemand unterwegs kann mitlesen.
2. Wir können sicher sein, dass wir mit dem genannten Server (resp. mit der angegebenen Webseite) kommunizieren.
3. Die übertragenen Inhalte sind vor Manipulation geschützt.

Wo ist trotzdem Vorsicht geboten?

1. Die URL (also die Webseitenadresse) muss ganz genau kontrolliert werden. Es nützt nichts, wenn die Verbindung verschlüsselt ist und der Server überprüft wurde, wenn wir mit dem falschen Server sprechen, weil wir zu wenig genau auf die URL achten.

   - **www.meinebank.com.hacker.com/login** ist keine gültige Webseite von MeineBank, sondern eine Unterseite von hacker.com.
   - **www.appenzeIler.ch** ist nicht dasselbe wie **www.appenzeller.ch** – auch wenn es fast identisch aussieht (achte genau auf die beiden «L»). In einer anderen Schriftart sieht man den Fehler besser: `www.appenzeIler.ch`

2. Die Webseite kann z.B. Malware verbreiten resp. gehackt worden sein. D.h. der «Inhalt» wird nicht überprüft.

[^1]: Quelle: [W3Techs](https://w3techs.com/technologies/details/ce-httpsdefault)
[^2]: Quelle: [internet live stats](https://www.internetlivestats.com/total-number-of-websites/)
