# Sichere Passwörter
---

<v-password-check/>

## Tipps für gute Passwörter

- Wähle für jede Webseite ein individuelles Passwort
- Das Passwort ist mindestens 10 Zeichen lang
- Es kommen Buchstaben (gross und klein), Zahlen und Sonderzeichen vor

## Gute, merkbare Passwörter

Es gibt Strategien, um gute Passwörter zu erstellen, die man sich einfach merken kann.

### A. Anfangsbuchstaben

**Jeden Abend vor dem Schlafen trinke ich eine Tasse Kräutertee!**

**JA4dStieTKt!**

### B. Vier zufällige Wörter

Wähle vier zufällige Wörter. Verwende beispielsweise einen Webdienst dazu. Überlege dir anschliessend eine Visualisierung, mit welcher du dir diese Wörter merken kannst.

### C. Individualisierung

Wähle ein gutes Passwort, welches du dir merken kannst. Füge für jeden Webdienst eine Zeichenfolge in dein Passwort ein, z.B. «ZA» für Zalando, «TM» für Teemondo:

| Webseite     | Passwort     |
| ------------ | ------------ |
| Zalando      | `loveZA4tea` |
| Ticketcorner | `loveTC4tea` |
| Teemondo     | `loveTM4tea` |


## Passwortmanager

Am sicherstent ist es, zufällige Passwörter zu verwenden, welche du in einem Passwortmanager verwaltest. So musst du dir nur ein Passwort merken, dasjenige für den Passwortmanager. Im Manager kannst du für jeden Dienst automatisch ein sicheres Passwort erzeugen lassen.

* [:link: **LastPass**: Online-Passwortmanager](https://www.lastpass.com/)
* [:link: **KeePassX**: Passwortmanager für Windows, macOS, Linux](https://www.keepassx.org/)
* [:link: **Password Safe**: Passwortmanager für Windows](https://www.pwsafe.org/)
