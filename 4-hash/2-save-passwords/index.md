# Passwörter speichern
---

Passwörter sind wichtig, wir sollten sie beschützen. Doch dies gilt nicht nur für unsere eigenen Passwörter, auch Firmen sollten die Passwörter ihrer Kunden schützen.

## Passwörter speichern
Wie würdest du die Passwörter deiner Kunden speichern, falls du einen Webshop erstellen müsstest? Natürlich nicht im Klartext, oder?

Dass die Passwörter «verschlüsselt» werden müssen, scheint logisch. Also sollten die Passwörter wohl auf dem Firmenserver verschlüsselt werden, damit sie nicht ausgelesen werden können – weder von Hackern noch von neugierigen Mitarbeitenden.

Doch gerade dies ist ein grosses Problem: Der Firmenserver soll die Kunden anhand ihres Passwortes authentifizieren können, die Mitarbeitenden dürfen allerdings keinen Zugriff auf die Kundenpasswörter haben. Alle bisherigen Verschlüsselungsverfahren arbeiten mit einem Schlüssel (oder mit mehreren). Wer den richtigen Schlüssel kennt, kann die Passwörter entschlüsseln. Wird der Schlüssel auf den Firmenservern gespeichert, so haben zumindest gewisse Mitarbeitende Zugriff.

Hier kommen wir momentan nicht weiter. Wir tasten uns von einer anderen Seite an die Lösung heran.

## Prüfsumme
Wir können doch – anstelle eines Passwortes – eine Prüfsumme speichern:

| Benutzername | Passwort   | Iterierte Quersumme | Alternierende Quersumme |
|:------------ |:---------- | ------------------- | ----------------------- |
| alice        | 12345      | 6                   | 3                       |
| bob          | 123456     |                     |                         |
| charlie      | 12345678   |                     |                         |
| donald       | 123456789  |                     |                         |
| eve          | 1234567890 |                     |                         |
| frederick    | 11111111   |                     |                         |
| grace        | 123123     |                     |                         |
| hanna        | password   |                     |                         |
| ian          | picture1   |                     |                         |

Die **iterierte Quersumme** ist die Quersumme, die entsteht, wenn man solange immer wieder die Quersumme ausrechnet, bis nur noch eine einzige Ziffer übrig bleibt. Für die Zahl `97` lautet die normale Quersumme `16`, berechnet man davon wiederum die Quersumme, so entsteht die iterierte Quersumme: `7`.

Die **Alternierende Quersumme** entsteht durch abwechslungweises Addieren und Subtrahieren der einzelnen Ziffern (für `1234` lautet diese `1 - 2 + 3 - 4 = -2`).

::: exercise Aufgabe Tabelle vervollständigen
1. Vervollständige die oben stehende Tabelle. Anstelle der in den Passwörtern enthaltenen Buchstaben kannst du die unten stehenden Zahlen einsetzen.
2. Welche der beiden Funktionen würdest du wählen, um die Passwörter zu speichern?
3. Welche Probleme siehst du bei beiden Funktionen?

| Buchstaben | `A B C D E F G H I _J _K _L _M _N _O _P _Q _R _S _T _U _V _W _X _Y _Z` |
| ---------- | ---------------------------------------------------------------------- |
| Zahlen     | `1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26` |

***
1.
   | Benutzername | Passwort   | Iterierte Quersumme | Alternierende Quersumme |
   | :----------- | :--------- | ------------------- | ----------------------- |
   | alice        | 12345      | 6                   | 3                       |
   | bob          | 123456     | 3                   | -3                      |
   | charlie      | 12345678   | 9                   | -4                      |
   | donald       | 123456789  | 9                   | 5                       |
   | eve          | 1234567890 | 9                   | 5                       |
   | frederick    | 111111     | 6                   | 0                       |
   | grace        | 123123     | 3                   | 0                       |
   | hanna        | password   | 7                   | 37                      |
   | ian          | picture1   | 3                   | -3                      |

2. Ganz klar die zweite Funktion (alternierende Quersumme). Bei der iterierten Quersumme sind nur 10 verschiedene Werte möglich, nämlich die Ziffern 0 - 9.
3. Wir möchten keine Kollisionen (d.h. mehrmals dieselbe Prüfsumme) erhalten.
:::

::: info Wunsch: Injektiviät
Zu jeder Prüfsumme gehört höchstens ein Passwort. Oder anders formuliert: **Kollisionen** sollen vermieden werden. Das bedeutet, dass jedes Passwort eine andere Prüfsumme erhalten soll.
:::

![Injektivität](./images/injective.png)

## Bessere Prüfsummen?!

Nachfolgend eine weitere Liste mit Vorschlägen für Prüfsummen.

| Benutzername | Passwort  | ???       | ???       |        ??? |
|:------------ |:--------- |:--------- |:--------- | ----------:|
| alice        | 1234567   | 7654321   | 3456789   |       5040 |
| bob          | qwerty    | ytrewq    | sygtva    |   17595000 |
| charlie      | abc123    | 321cba    | cde345    |         36 |
| donald       | million2  | 2noillim  | oknnkqp4  |   63685440 |
| eve          | 000000    | 000000    | 222222    |          0 |
| frederick    | 1234      | 4321      | 3456      |         24 |
| grace        | iloveyou  | uoyevoli  | knqygaqw  | 1403325000 |
| hanna        | password1 | 1drowssap | rcuuyqtf3 |  143475840 |
| ian          | qqww1122  | 2211wwqq  | ssyy3344  |     611524 |

::: exercise Aufgabe Funktionen erkennen
Erkennst du, wie die Prüfsummen in den hintersten drei Spalten berechnet werden?
***
| Benutzername | Passwort  | rückwärts | überall +2 | Werte multiplizieren |
|:------------ |:--------- |:--------- |:---------- | --------------------:|
| alice        | 1234567   | 7654321   | 3456789    |                 5040 |
| bob          | qwerty    | ytrewq    | sygtva     |             17595000 |
| charlie      | abc123    | 321cba    | cde345     |                   36 |
| donald       | million2  | 2noillim  | oknnkqp4   |             63685440 |
| eve          | 000000    | 000000    | 222222     |                    0 |
| frederick    | 1234      | 4321      | 3456       |                   24 |
| grace        | iloveyou  | uoyevoli  | knqygaqw   |           1403325000 |
| hanna        | password1 | 1drowssap | rcuuyqtf3  |            143475840 |
| ian          | qqww1122  | 2211wwqq  | ssyy3344   |               611524 |
:::

::: info Wunsch: Unumkehrbarkeit
Aus der Prüfsumme soll nicht auf das Passwort geschlossen werden können.
:::

::: exercise Zusatzaufgabe Sicheres Einloggen
Wie funktioniert der Anmeldevorgang an den Computern der Schule, wenn Benutzername und Passwort von einem Server überprüft werden muss? Was wird in welcher Form übertragen?
:::
