# Vigenère-Chiffre
---

Die Vigenère-Chiffre wurde vom Franzosen Blaise de Vigenère (1523-1596) erfunden und galt fast 300 Jahre lang als unknackbar. Es handelt sich um ein *polyalphabetisches* Verschlüsselungsverfahren, da mehrere Alphabete genutzt werden. Als Schlüssel dient ein Wort, das die Anzahl der verwendeten Substitutions-Alphabete bestimmt: Für jeden Buchstaben des Schlüsselwortes wird die Caesar-Chiffre mit dem jeweiligen Buchstaben als Schlüssel verwendet. Die Buchstaben des Klartextes werden abwechslungsweise durch diese Caesar-Chiffren verschlüsselt.

* [:link: Vigenère-Verschlüsselung Online](https://gc.de/gc/vigenere/)

![Vigenère-Verschlüsselung ©](./images/vigenere-example.png)

::: exercise Zusatzaufgabe
Verschlüssle den Text `HEUTE UM FUENF UHR` mit der Vigenère-Chiffre. Verwende dazu den Schlüssel `JANUS`.

***

`QEHNW DM SOWWF HBJ`
:::

::: exercise Zusatzaufgabe
Überlege dir, wie man die Vigenère-Chriffre angreifen könnte.
:::
