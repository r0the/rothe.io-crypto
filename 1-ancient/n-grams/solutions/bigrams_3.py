import math

def lade_datenbank(dateiname):
    # Absolute Häufigkeiten der Bigramme aus Datei lesen
    datei = open(dateiname, encoding="utf-8")
    total = 0
    datenbank = {}
    for zeile in datei:
        eintrag = zeile.split(' ')
        bigramm = eintrag[0]
        anzahl = int(eintrag[1])
        total = total + anzahl
        datenbank[bigramm] = anzahl
    datei.close()
    # Logarithmus der relativen Häufigkeiten berechnen
    for bigramm in datenbank:
        datenbank[bigramm] = math.log(datenbank[bigramm] / total)
    return datenbank

# Bigramme eines Worts bestimmen
def bigramme(wort):
    liste = []
    letztes = ""
    for zeichen in wort:
        if len(letztes) > 0:
            liste.append(letztes + zeichen)
        letztes = zeichen
    return liste

# Fitness eines Worts berechnen
def fitness_wort(datenbank, wort):
    resultat = 0
    for bigramm in bigramme(wort):
        if bigramm in datenbank:
            resultat = resultat + datenbank[bigramm]
    return resultat

db = lade_datenbank("german_bigrams.txt")
print("Fitness von HALLO:", fitness_wort(db, "HALLO"))
print("Fitness von KJEXG:", fitness_wort(db, "KJEXG"))
