import math

def lade_datenbank(dateiname):
    # Absolute Häufigkeiten der Bigramme aus Datei lesen
    datei = open(dateiname, encoding="utf-8")
    total = 0
    datenbank = {}
    for zeile in datei:
        eintrag = zeile.split(' ')
        bigramm = eintrag[0]
        anzahl = int(eintrag[1])
        total = total + anzahl
        datenbank[bigramm] = anzahl
    datei.close()
    # Logarithmus der relativen Häufigkeiten berechnen
    for bigramm in datenbank:
        datenbank[bigramm] = math.log(datenbank[bigramm] / total)
    return datenbank

db = lade_datenbank("german_bigrams.txt")
for bigramm in db:
    print(bigramm, db[bigramm])
