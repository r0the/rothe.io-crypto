# 1.4 Begriffe
---

## Kryptologie

Der Begriff **Kryptologie** setzt sich zusammen aus dem altgriechischen *κρυπτός* (also *kryptós*), was **verborgen** bedeutet, und *λόγος* (also *lógos*), was
**Lehre**, **Kunde** bedeutet. Kryptologie bezeichnet die Wissenschaft, die sich mit der Ver- und Entschlüsselung von Informationen (also mit **Informationssicherheit**) beschäftigt.

![Kryptologie, Kryptographie und Kryptoanalyse](./images/cryptology.svg)

## Kryptographie und Kryptoanalyse

Die Kryptologie kann grob in zwei Teilbereiche unterteilt werden:

- die **Kryptographie** (*γράφειν*, also *gráphein*, was **schreiben** bedeutet) und
- die **Kryptoanalyse**.

Während sich die Kryptographie mit dem Finden von sicheren Verschlüsselungsverfahren beschäftigt, liegt der Fokus der Kryptoanalyse beim «Brechen» oder «Knacken» solcher Verfahren.

## Klartext, Geheimtext und Schlüssel

| Symbol | deutscher Begriff | englischer Begriff | Bedeutung                                                                                       |
| ------ |:----------------- |:------------------ |:----------------------------------------------------------------------------------------------- |
| $p$    | **Klartext**      | *plaintext*        | unverschlüsselte Nachricht                                                                      |
| $c$    | **Geheimtext**    | *ciphertext*       | verschlüsselte Nachricht                                                                        |
| $k$    | **Schlüssel**     | *key*              | Information, welche benötigt wird, um den Klartext zu ver- bzw. den Geheimtext zu entschlüsseln |


## Verschlüsselungsverfahren

Ein **Verschlüsselungsverfahren** ist ein Algorithmus, welche einen Klartext unter Verwendung eines Schlüssels in einen Geheimtext überführt oder umgekehrt.

![Zusammenhang zwischen Klartext, Geheimtext und Schlüssel](./images/symmetric-cryptosystem.svg)

## Schlüsselraum

Der Begriff Schlüsselraum bezeichnet die Anzahl möglicher Schlüssel für ein bestimmtes Verschlüsselungsverfahren. Die Sicherheit eines Verschlüsselungsverfahrens hängt stark von der Grösse des Schlüsselraums ab.

Die Grösse des Schlüsselraums wird in Bit (d.h. als binärer Logarithmus) angegeben. Bei zwei möglichen Schlüsseln spricht man von 1 Bit, bei 1024 möglichen Schlüsseln von 10 Bit.

::: exercise Aufgabe Schlüsselräume

Gib die Grösse des Schlüsselraumes für die folgenden Verschlüsselungsverfahren an:

- Skytale
- Caesar
- ROT13
- allgemeine monoalphabetische Substitution

:::
