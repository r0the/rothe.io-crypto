# 1 Antike Verschlüsselungsverfahren
---

* [Skytale](?page=1-skytale/)
* [Polybios-Chiffre](?page=2-polybios/)
* [Caesar-Chiffre](?page=3-caesar/)
* [Monoalphabetische Substitution](?page=4-substitution/)
* [Begriffe](?page=5-concepts/)
* [Häufigkeitsanalyse](?page=6-al-kindi/)
* [:extra: Vigenère-Chiffre](?page=vigenere/)
* [:extra: n-Gramme](?page=n-grams/)
