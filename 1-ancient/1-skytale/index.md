# Skytale
---

Um ca. 500 v. Chr. nutzten die Spartaner eine Skytale (griechisch für «Stock» oder «Stab»), um wichtige militärische Botschaften zu übermitteln. Der Absender wickelte dabei einen Streifen aus Pergament oder Leder spiralförmig um die Skytale und schrieb die Nachricht längs des Stabes auf das aufgewickelte Band. Auf dem abgewickelten Streifen, der dem Empfänger übermittelt wird, steht nun eine scheinbar sinnlose Buchstabenfolge. Der Empfänger kann die Botschaft mit einer Skytale vom selben Durchmesser aber wieder entziffern.

![Nachbildung einer Skytale ©](./images/skytale.png)

## Transposition

Die Skytale ist ein Beispiel einer Verschlüsselung durch **Transposition**. Das heisst, dass die Zeichen des Geheimtextes nicht ersetzt, sondern nur umgestellt werden.

## Skytale ausprobieren

<v-skytale/>
