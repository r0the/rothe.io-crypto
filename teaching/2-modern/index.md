# 2. Doppellektion – Symmetrisch
---

* [:pdf: Präsentation Blockchiffre](./2-1-Kryptologie-Blockchiffre.pdf)
* [:pptx: Präsentation Blockchiffre](./2-1-Kryptologie-Blockchiffre.pptx)
* [:pdf: Aufgabe XOR-Chiffre](./2-1-Aufgabe-XOR-Chriffe.pdf)
* [:docx: Aufgabe XOR-Chiffre](./2-1-Aufgabe-XOR-Chriffe.docx)
* [:pdf: Präsentation Krypto-Familie](./2-2-Krypto-Familie.pdf)
* [:pptx: Präsentation Krypto-Familie](./2-2-Krypto-Familie.pptx)
* [:pdf: Präsentation Kerckhoffs' Prinzip](./2-3-Kryptologie-Kerckhoffs.pdf)
* [:pptx: Präsentation Kerckhoffs' Prinzip](./2-3-Kryptologie-Kerckhoffs.pptx)
