# 1. Doppellektion – Einstieg
---

1. Die SuS versuchen ohne die Hilfe der Webseite die vorliegenden verschlüsselten Texte zu entschlüsseln.
2. Damit sie nicht zu lange im Dunkeln tappen, kann den SuS nach einiger Zeit der Name des Algorithmus genannt werden. Zudem kann ihnen die Webseite mit den zusätzlichen Informationen und den Tools gezeigt werden.
3. Diskussion über die verschiedenen Algorithmen.
4. Abschluss der Doppellektion: Präsentation.

* [:pdf: Caesar-Scheibe](./caesar-scheibe.pdf)
* [:pdf: Verschlüsselte Texte](./1-1-Verschlüsselte-Texte.pdf)
* [:docx: Verschlüsselte Texte](./1-1-Verschlüsselte-Texte.docx)
* [:pdf: Verschlüsselte Texte – Übersicht und Lösungen](./1-1-Verschlüsselte-Texte-Lösung.pdf)
* [:docx: Verschlüsselte Texte – Übersicht und Lösungen](./1-1-Verschlüsselte-Texte-Lösung.docx)
* [:pdf: Einführungspräsentation](./1-2-Kryptologie-Grundlagen.pdf)
* [:pptx: Einführungspräsentation](./1-2-Kryptologie-Grundlagen.pptx)
