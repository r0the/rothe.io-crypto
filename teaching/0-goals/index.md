# :goal: Lernziele
---

::: goal Lernziele Verschlüsselungsverfahren
- Du verstehst das Prinzip symmetrischer Verschlüsselungsverfahren und wendest das Wissen in Beispielen mit antiken Verfahren an.
- Du greifst antike symmetrische Verfahren an (Häufigkeitsanalysen, Brute Force, ...).
- Du kennst das Prinzip von Kerckhoffs und kannst nachvollziehen, wieso es sinnvoll ist, die Funktionsweise von Algorithmen nicht geheimzuhalten (Security through Obscurity).
- Du weisst, dass moderne symmetrische Verfahren mit Bitfolgen anstelle von Buchstaben arbeiten und mathematisch viel komplexer sind.
- Du erkennst das Problem des Schlüsselaustauschs bei symmetrischen Verfahren.
- Du kennst Man-in-the-Middle-Angriffe und verstehst, dass auch öffentliche Schlüssel auf ihre Echtheit überprüft werden müssen.
- Du begreifst asymmetrische Verfahren mit ihren Schlüsselpaaren als Hilfsmittel für einen sicheren Austausch (z.B. Vorhängeschloss/Schlüssel-Analogie).
- Du verstehst Zertifikate als Bestätigung der Echtheit von öffentlichen Schlüsseln durch eine vertrauenswürdige Stelle mit Hilfe einer digitalen Signatur (z.B. bei HTTPS).
:::

::: goal Lernziele Hashfunktionen
- Du weisst, dass Passwörter nicht im Klartext gespeichert werden.
- Du verstehst Hashverfahren als Möglichkeit zur sicheren Speicherung von Passwörtern und zur Intregritätsprüfung.
- Du kennst Angriffsmöglichkeiten (Dictionnary, Brute Force) auf Passworthashes.
- Du kannst die Stärke von Passwörtern einschätzen und weisst, dass die Länge die massgebende Grösse für ein sicheres Passwort ist.
:::

::: goal Lernziele Digitale Signaturen
- Du verstehst, dass das Prinzip der asymmetrischen Verschlüsselung auch für digitale Signaturen eingesetzt werden kann.
- Du verstehst die Rolle von Zertifizierungsstellen und kannst nachvollziehen, was Webseitenzertifikate bedeuten.
:::

::: goal Lernziele Angriffe
- Du bist dir bewusst, dass für erfolgreiche Angriffe auf Systeme häufig Fehlverhalten von Personen verantwortlich ist.
- Du weisst, dass Personen daher ein beliebtes Angriffsziel sind (z.B. Social Engineering, Phishing).
- Du weisst, dass Systeme Schwachstellen aufweisen und somit Angriffsflächen bieten.
- Du verstehst, dass regelmässige Softwareupdates, aktuelle Antivirensoftware und eine aktive Firewall für die Systemsicherheit zentral sind.
- Du verstehst die Wichtigkeit, dieselben Passwörter nicht für mehrere Dienste/Systeme einzusetzen und kennst Möglichkeiten der Passwortverwaltung (z.B. Passwortmanager).
- Du verstehst, dass das Passwort des E-Mail-Accounts besonders wichtig ist (z.B. wegen der Passwortrücksetzung via E-Mail).
:::


## Grundlagen

Diese Unterrichtseinheit deckt die folgenden Grobziele und Inhalte aus dem kantonalen Lehrplan[^1] ab:

> ### Information und Daten
>
> #### Grobziele
> Die Schülerinnen und Schüler
> - haben Kenntnisse über Angriffsflächen von Systemen und Kommunikationskanälen sowie Schutzmechanismen
>
> #### Inhalte
> - Aktuelle Angriffsmethoden (z.B. Social Engineering, Brute Force, DoS, Malware)
> - Grundlagen von Sicherheit und Kryptografie (z.B. Vertraulichkeit, Schutz vor Manipulation, digitale Signatur)

[^1]: Quelle: *[Lehrplan 17 für den gymnasialen Bildungsgang - Informatik][1], Erziehungsdirektion des Kantons Bern, S. 145 - 146*

[1]: https://www.erz.be.ch/erz/de/index/mittelschule/mittelschule/gymnasium/lehrplan_maturitaetsausbildung.html
