# Material für Lehrer*innen
---

## Möglicher Ablauf (überarbeitet 2021)

* [1. Doppellektion – Einstieg](?page=1-ancient/)
* [2. Doppellektion – Symmetrisch](?page=2-modern/)
* [3. Doppellektion – Asymmetrisch](?page=3-publickey/)
* [4. Doppellektion – Hashfunktionen](?page=4-hash/)
* [5. Doppellektion – Digitale Signaturen](?page=5-signature/)
