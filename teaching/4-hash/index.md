# 4. Doppellektion – Hashfunktionen
---

## Authentifizierung

* [:pdf: Präsentation Authentifizierung](./4-0-Authentifizierung.pdf)
* [:pptx: Präsentation Authentifizierung](./4-0-Authentifizierung.pptx)

## Einführung Hashfunktionen

* [:pdf: Präsentation Hashfunktionen und Passwörter](./4-1-Hashfunktionen-Passwoerter.pdf)
* [:pptx: Präsentation Hashfunktionen und Passwörter](./4-1-Hashfunktionen-Passwoerter.pptx)

basiert auf der Präsentation von paa, gut eine Lektion

- Was denken Sie, was sind die am meisten verwendeten Passwörter?
- Wie kommen “Hacker” an Passwörter?
- -> Nicht Passwort speichern, sondern Hashwert
- Auftrag 1 (Quersummen) -> Kollisionsresistenz
- Auftrag 2 (Summe, Multiplikation) -> Einwegfunktion

* [:pdf: Auftrag 1](./4-2-Auftrag.pdf)
* [:docx: Auftrag 1](./4-2-Auftrag.docx)
* [:pdf: Auftrag 2](./4-3-Auftrag.pdf)
* [:docx: Auftrag 2](./4-3-Auftrag.docx)

## Passwortsicherheit

Alice benutzt überall das gleiche Passwort, Story nach jat.

* [:pdf: Präsentation Passwörter (ros)](./4-4-Passwoerter.pdf)
* [:pptx: Präsentation Passwörter (ros)](./4-4-Passwoerter.pptx)
