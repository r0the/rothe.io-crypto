# 5. Doppellektion – Digitale Signaturen
---

* [:pdf: Präsentation Digitale Signatur](./5-1-Digitale-Signatur.pdf)
* [:pptx: Präsentation Digitale Signatur](./5-1-Digitale-Signatur.pptx)
* [:pdf: Von der digitalen Signatur zum Zertifikat](./5-2-Von-der-digitalen-Signatur-zum-Zertifikat.pdf)
* [:docx: Von der digitalen Signatur zum Zertifikat](./5-2-Von-der-digitalen-Signatur-zum-Zertifikat.docx)
* [:pdf: Präsentation Zertifikate](./5-3-Zertifikate.pdf)
* [:pptx: Präsentation Zertifikate](./5-3-Zertifikate.pptx)
* [:pdf: Präsentation Krypto-Zusammenfassung](./5-4-Krypto-Zusammenfassung.pdf)
* [:pptx: Präsentation Krypto-Zusammenfassung](./5-4-Krypto-Zusammenfassung.pptx)
