# 3. Doppellektion – Asymmetrisch
---

* [:pptx: Präsentation Schlüsselaustausch](./3-1-Schlüsselaustausch.pptx)
* [:pdf: Präsentation Schlüsselaustausch](./3-1-Schlüsselaustausch.pdf)
* [:pptx: Präsentation Asymmetrische Verschlüsselung](./3-2-Asymmetrische-Verschlüsselung.pptx)
* [:pdf: Präsentation Asymmetrische Verschlüsselung](./3-2-Asymmetrische-Verschlüsselung.pdf)
* [:pdf: Aufgabe Repetition Begriffe](./3-3-Repetition-Begriffe.pdf)
* [:docx: Aufgabe Repetition Begriffe](./3-3-Repetition-Begriffe.docx)
