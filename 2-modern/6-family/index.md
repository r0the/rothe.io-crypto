# Krypto-Familie
---

::: cards 3
![Alice ©](./images/alice.svg)

Alice und Bob kommunizieren häufig miteinander – dies natürlich verschlüsselt.
***
![Bob ©](./images/bob.svg)

Anstelle von «Person A» und «Person B» spricht man von Alice und Bob.
***
![Charlie ©](./images/charlie.svg)

Wird eine dritte Person benötigt, kommt Charlie ins Spiel.
***
![Eve ©](./images/eve.svg)

Eve wird meist dann eingesetzt, wenn Verbindungen abgehört (engl. **eave***sdropping*) werden. Sie ist also eine passive Zuhörerin.
***
![Mallory ©](./images/mallory.svg)

Mallory ist ein bösartiger, aktiver Angreifer (engl. **mal***icious*).
***
![Trent ©](./images/trent.svg)

Wird eine vertrauenswürdige dritte Stelle verwendet, wird Trent (engl. für **tr***usted* **ent***ity*) eingesetzt.
:::


## Symmetrische Verschlüsselung

Bei sämtlichen bisher betrachteten Verfahren handelte es sich um symmetrische Verfahren, d.h. zum Verschlüsseln und Entschlüsseln wird derselbe Schlüssel verwendet:

![©](./images/symm-encryption.svg)
