# One-Time-Pad
---

::: info
Das One-Time-Pad ist ein Verschlüsselungsverfahren, das beweisbar **perfekte Sicherheit** bietet.
:::

Erfunden wurde das One-Time-Pad bereits 1882, richtig bekannt wurde es allerdings erst 1917, als es von *Gilbert Vernam* zum Patent angemeldet wurde. Daher wird es häufig auch **Vernam One-Time-Pad** genannt.

Als erstes müssen die Buchstaben des Klartextes und des Schlüssels in Zahlen umgewandelt:

|      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| `A`  | `B`  | `C`  | `D`  | `E`  | `F`  | `G`  | `H`  | `I`  | `J`  | `K`  | `L`  | `M`  |
| `_0` | `_1` | `_2` | `_3` | `_4` | `_5` | `_6` | `_7` | `_8` | `_9` | `10` | `11` | `12` |

|      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| `N`  | `O`  | `P`  | `Q`  | `R`  | `S`  | `T`  | `U`  | `V`  | `W`  | `X`  | `Y`  | `Z`  |
| `13` | `14` | `15` | `16` | `17` | `18` | `19` | `20` | `21` | `22` | `23` | `24` | `25` |

Zum **Verschlüsseln** addierst du die zum jeweiligen Klartextbuchstaben gehörende Zahl zur Zahl des Schlüsselbuchstabens. Falls das Resultat zu gross wird, ziehst du 26 ab (genannt *Addition modulo 26*). Nun suchst du den Buchstaben, der zur ausgerechneten Zahl passt:

::: columns 2
|            |          |
| :--------- | :------- |
| Klartext   | `E =  4` |
|            | `     +` |
| Schlüssel  | `M = 12` |
| Geheimtext | `Q = 16` |
***
|            |                   |
| :--------- | :---------------- |
| Klartext   | `R = 17`          |
|            | `     +`          |
| Schlüssel  | `K = 10`          |
| Geheimtext | `B = 27 - 26 = 1` |
:::

Die Entschlüsselung läuft genau umgekehrt. Anstatt den Schlüsselbuchstaben zu addieren, subtrahierst du ihn. Falls das Resultat kleiner als 0 wird, addierst du 26:

::: columns 2
|            |          |
| :--------- | :------- |
| Geheimtext | `T = 19` |
|            | `     -` |
| Schlüssel  | `F =  5` |
| Klartext   | `O = 14` |
***
|            |                    |
| :--------- | :----------------- |
| Geheimtext | `G =  6`           |
|            | `     -`           |
| Schlüssel  | `J =  9`           |
| Klartext   | `X = -3 + 26 = 23` |
:::

::: exercise Aufgabe One-Time-Pad
Gegeben sei der folgende Geheimtext. Versuche, ihn zu entschlüsseln.

``` code
NJXOG PMNQD UGOQP GAEBI PLFCP EXEKZ NCLWS KTATE XKOYA YPBPH KFSLV JWJNF MLHME
BDXTH TTRQU QAEYN SVBIZ YVOCU KWNMF AODZJ PFJGA PDSLG XOUNH XAYCS SBNRF UVZID
VJOGR VKZCG YNYWP TGRRW SCQAF JHQLQ LRQWO VMDXM IEUIY TCJXR BUMNR ORUKU YXFSP
TNFWC FIVLI YVINT OIWHI DGXRD FBHKM ELRZF UUVDG ATGIL GJJME CHSCI QSDSW EHOCK
ZSCBV FOXJG HUQDI NUWXK EJPHK CSTBQ LAUAZ DYZAW CAITO ERMKW LWZKF BRAJZ GZZVJ
BQBMX EPHBN XMYQM LDVPH VSTWQ TDMKO BYRYZ EPLH
```

Führe eine Häufigkeitsanalyse durch, damit du den Text entschlüsseln kannst.
:::

::: exercise Aufgabe One-Time-Pad Fortsetzung
Versuche, den Text aus der vorherigen Aufgabe zu entschlüsseln, indem du den folgenden Schlüssel verwendest:

``` code
JBKHG LSIIX QPODJ PSZWB LDNKW DGKRV IOUUO YPHMQ UGHQW HOXHL GOPHI WWHGB EYHZB
XMFTD HAGIS JWSKJ MKTGS UIWAN ZCJUN WDAFS NYUPM OVOUN FMNAD MPULK AIGRB AQRCZ
NWSSN ERVLF ELRWC NPJMR RYIXB XDIYM TREKD BZXCY VPUQG XOFGY XDZKX XPNVD KWXOY
ARXFZ EENZQ UIFJG TUJZV YSGFD MTTXI RRNYB DAIXC IRZOH NKQIU CUSYX MGJAE MOKJS
WSQTC ZKGFE AHMKM JDTTX BJXPG UFTOK UWMVV MEMHS LEENW SJTDI HFGWC XAIVT GIVNW
VZXES LMZXV KILDT ZDIDH IKGDJ PREHL QURLT NHGC
```

Was passiert, wenn du den folgenden Schlüssel verwendest?

``` code
RBGWY CJJIQ OICDP OSKPO CIEYY APLGM NIGTK GMMRX FIHEP UCGBQ HXOLB RVBCC SYBMS
VFLGH BLXEK IJCRJ FQXXW GCKRJ RTFIE JKVGF PUYAW DZKYF PDRTU RIQAL OKKJB RRMJJ
PJBAS BKORC LVFCM LCELW FWMNF WNDDV HAYOV VTZKS VBNUW MKHQX QQZZM JEQRF HJAKE
LJODG XRDHP ZRVVR HMSQT JTNYZ FOPVV KJKHK GJKHY JSYES CWOEA RNFZM IBIOF THBWG
MXUXK GUNPT CBYXE WMUQR AQTZT KKGYC GVQNF QVMWP QWVMK NRSSR XFWGO HEUFM GMIRR
MMRTC QEWFF GLUKI FQRCN IAHOX XZVRW ZRRUG FVYB
```

Kannst du erklären, was hier passiert ist? Wieso gibt es dieses erstaunliche Resultat?
:::
