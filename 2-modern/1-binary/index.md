# Umwandlung ins Binärsystem
---

In diesem Kapitel dringen wir in unsere Zeit vor. Wir verwenden Computer, die mit binären Zahlen arbeiten. Somit muss der Klartext vor der Verschlüsselung in binäre Zahlen umgewandelt werden.

## Ein kleiner Nachteil
Das Verfahren wird daher etwas umständlicher:

- Codierung des Geheimtextes in eine Folge von binären Zahlen (Klartext → binärer Klartext)
- Verschlüsselung der dieser Zahlenfolge (binärer Klartext → binärer Geheimtext)
- Decodierung des binären Geheimtextes (binärer Geheimtext → Geheimtext)

::: info Unterschied «Codierung» und «Verschlüsselung»
Es ist wichtig, dass wir die Begriffe **Codierung** und **Verschlüsselung** unterscheiden:

- Eine **Codierung** ist eine *Umwandlung* in eine andere Darstellungsform (z.B. Text → Zahlen, Bild → Text, Zahlen → binäre Zahlen, ...). Eine Codierung hat das Ziel, eine Information in ein bestimmtes Datenformat umzuwandeln, sie bietet **keine Sicherheit**.
- Eine **Verschlüsselung** oder **Chiffre** hat das Ziel zu verhindern, dass Aussenstehende an eine bestimmte Information gelangen, indem Daten mithilfe eines Schlüssels so verändert werden, dass ohne Kenntnis des Schlüssels nicht mehr auf die ursprüngliche Form geschlossen werden kann.
:::

## Ein grosser Vorteil
Die vorgängige Codierung bietet auch einen erheblichen Vorteil. Alles, was sich in binärer Form darstellen lässt, kann ohne weitere Anpassung des Verfahrens verschlüsselt werden:

- Texte (unabhängig von der Sprache resp. den verwendeten Schriftzeichen)
- Bilder
- Ton
- Videos

Dies war in der Antike nicht nötig, heute ist es allerdings unumgänglich.

## Codierung

Wie im Kapitel [«Codes und Daten»](?book=code) besprochen, gibt es dazu eine Vielzahl von Codierungen. Beispielsweise könnte dazu die [ASCII-Codierung](?book=code&p=647751) verwendet werden.

Für die folgenden Beispiele verwenden wir den etwas kürzeren [Pentacode](?book=code&p=73657).
