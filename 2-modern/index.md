# 2 Symmetrische Verschlüsselungsverfahren
---

* [Umwandlung ins Binärsystem](?page=1-binary/)
* [:interactive: Codierungen](?page=2-encoding/)
* [XOR-Verschlüsselung](?page=3-xor/)
* [Blockchiffre](?page=4-block-cipher/)
* [Verkettung der Blöcke](?page=5-block-chaining/)
* [Krypto-Familie](?page=6-family/)
* [Kerckhoffs' Prinzip](?page=7-kerckhoffs/)
* [:extra: One-Time-Pad](?page=one-time-pad/)
