# XOR-Verschlüsselung
---

Da wir nun mit Bits arbeiten und nicht mehr mit Buchstaben, müssen wir nach anderen Möglichkeiten zur Verschlüsselung suchen. Alphabetverschiebungen wie bei Caesar und Substitutionen sind nicht mehr geeignete Mittel, wenn nur die Zahlen 0 und 1 zur Verfügung stehen.

Wir kennen allerdings bereits etliche Operationen, die mit binären Zahlen ausgeführt werden können aus dem Kapitel [Computer](?book=computer&page=computer/3-gates/). Eine besonders einfache und geeignete Operation ist **XOR** (siehe Kapitel [Computer](?book=computer&page=computer/3-gates/6-xor-gate/)). XOR kann auch zur Verschlüsselung verwendet werden. Dabei wird jeweils 1 Bit des Klartextes mit einem Bit des Schlüssels verrechnet. Das Ergebnis ist 1 Bit des Geheimtextes.

Die Wahrheitstabelle sieht folgendermassen aus:

::: cards 2

**Verschlüsselung**

| $p$ | $k$ | $c$ = $p$ XOR $k$ |
|:---:|:---:|:-----------------:|
|  0  |  0  |         0         |
|  0  |  1  |         1         |
|  1  |  0  |         1         |
|  1  |  1  |         0         |

***

**Entschlüsselung**

| $c$ | $k$ | $p$ = $c$ XOR $k$ |
|:---:|:---:|:-----------------:|
|  0  |  0  |         0         |
|  1  |  1  |         0         |
|  1  |  0  |         1         |
|  0  |  1  |         1         |
:::

Analog zu ROT13 gilt auch hier: Die Verschlüsselung ist identisch mit der Entschlüsselung, da Folgendes gilt:

$$c \text{ XOR } k = (p \text{ XOR } k) \text{ XOR } k = p$$

Dies kannst du in den Tabellen oben leicht überprüfen.

::: exercise Aufgabe XOR-Verschlüsselung
Verschlüssle die Bitfolge mit dem angegebenen Schlüssel:

`10100 11101 10110` (Bitfolge des Klartexts)

`00101 01010 11100` (Bitfolge des Schlüssels)

***

`10001 10111 01010` (Bitfolge des Geheimtexts)
:::

::: exercise Aufgabe Textverschlüsselung mit XOR
Du erhältst von einer Kollegin folgende verschlüsselte Nachricht:

`01010 00100 01010 11011 00000`

Als Schlüssel habt ihr das Wort `MACHT` abgemacht.

1. Codiere den Schlüssel mit Pentacode und entschlüssle die Nachricht.
2. Wandle die entschlüsselte Bitfolge mit Hilfe von Pentacode wieder in Text um.

***

`01101 00001 00011 01000 10100` (Bitfolge des Schlüssels)

`00111 00101 01001 10011 10100` (entschlüsselte Bitfolge)

`GEIST` (Klartext)
:::

::: exercise Aufgabe Bildverschlüsselung mit XOR
1. Zeichne ein 5x5-Pixel-Bild (mit Hilfe des interaktiven Tools auf der vorherigen Seite) und verschlüssle die angezeigte Bitfolge mit dem Schlüssel `ABCDE`.
2. Wandle die resultierende Bitfolge wieder in ein Bild um.
3. Was geschieht, wenn du daraus mit Pentacode einen Text erstellst?
:::
