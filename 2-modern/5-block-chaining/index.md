# Verkettung der Blöcke
---

Im letzten Kapitel wurde der Geheimtext zur Ver- resp. Entschlüsselung in Blöcke aufgeteilt, die exakt so lang sind wie der Schlüssel. Die Blöcke werden anschliessend einzeln durch die XOR-Funktion mit dem Schlüssel ver- resp. entschlüsselt.

::: info Allgemeine Blockchiffre
Prinzipiell kann anstelle der XOR-Funktion jede beliebige mathematische Funktion (resp. Verkettung von Funktionen) zur verwendet werden (in den unten aufgeführten Grafiken daher mit dem allgemeinen Begriff *block cipher encryption* bezeichnet). Wir beschränken uns hier allerdings auf die einfache und gut verständliche XOR-Verschlüsselung.
:::

## Der Betriebsmodus «Electronic Code Book» (ECB)
Falls jeder Block mit dem immer gleichen Schlüssel verschlüsselt wird, spricht man vom «Electronic Code Book» (ECB) Betriebsmodus. Schematisch dargestellt sieht dieser wie folgt aus:

![Verschlüsselung im ECB-Modus ©](./images/ecb_encryption.svg)

In der Abbildung wird deutlich, dass ECB kein eigenes Verschlüsselungsverfahren, sondern nur ein Modus ist. Er definiert, wie die einzelnen Blöcke verwendet werden. Die konkrete Verschlüsselungsfunktion ist in dieser Darstellung nicht genauer angegeben – wir verwenden der Einfachheit halber hier die XOR-Funktion.

Die Entschlüsselung funktioniert analog: Der Geheimtext wird wiederum in Blöcke aufgeteilt, welche separat mit dem Schlüssel entschlüsselt werden. Die dabei entstandenen Klartext-Blöcke ergeben aneinander gereiht den gesamten Klartext.

![Entschlüsselung im ECB-Modus ©](./images/ecb_decryption.svg)

<v-xor-block-cipher/>

::: exercise Aufgabe ECB-Modus
1. Verschlüssle den folgenden Text `EINE SEHR KLEINE SENSATION` mit dem Schlüssel `ZUSE`.
2. Schaue dir den Klartext und den Geheimtext genau an.
3. Überlege dir, welche Schwächen der ECB-Modus hat.

***

Überlege dir, was passiert, wenn zwei Blöcke identisch sind (z.B. weil eine bestimmte Passage des Textes erneut vorkommt). Wie ist die Auswirkung auf den Geheimtext?

***
1. Geheimtext: `@-. ZFVMHUXI@-. ZFVKITGLU,`
2. Die Blöcke `EINE` (am Anfang des Klartextes und im Wort `KLEINE`) ergeben exakt denselben Geheimtextblock `@-. `. Beim jeweils darauf folgenden Block stimmen die ersten 3 Zeichen überein – sowohl im Klartext (` SE`) wie auch im Geheimtext (`ZFV`).
3. Folgende Schwächen fallen auf:
   1. Jeder Klartext-Block wird mit demselben Schlüssel verschlüsselt. Gleiche Klartext-Blöcke ergeben demnach gleiche Geheimtext-Blöcke, was bereits viel über den Klartext aussagt.
   2. Eine Änderung in einem Klartext-Block führt nur zu einer Änderung im entsprechenden Geheimtext-Block. Unterscheiden sich zwei mit demselben Schlüssel   verschlüsselte Geheimtexte nur minim, sind auch die Klartexte fast identisch.
   3. Ein Angreifer kann eine abgefangene, verschlüsselte Nachricht verändern, z.B. indem er Blöcke einfügt, die mit demselben Schlüssel verschlüsselt worden sind, ohne dass der Empfänger dies merkt. Der Angreifer muss dabei den Schlüssel nicht kennen, er kann einfach bestehende Geheimtextblöcke verwenden.
:::

## Der Betriebsmodus «Cipher Block Chaining» (CBC)

Im CBC-Modus werden die Blöcke nicht mehr getrennt voneinander verarbeitet. Wie in der folgenden Abbildung ersichtlich ist, dient jeder Geheimtext-Block (ausser der letzte) im nachfolgenden Schritt zusätzlich als Input. So werden gleiche Klartext-Blöcke trotz identischem Schlüssel zu unterschiedlichen Geheimtextblöcken
verschlüsselt.

Das Plus-Zeichen ich Kreis steht hier ebenfalls für die XOR-Operation. Diese ist gegeben, während die Verschlüsselung im grossen Rechteck mit der Bezeichnung *block cipher encryption* aus aktuell als sicher geltenden Verfahren frei gewählt werden kann. Es wird also in unseren Beispielen sowohl für die Verrechnung des Klartextblocks mit dem vorherigen Geheimtextblock wie auch für eigentliche Verschlüsselung die XOR-Operation verwendet.

Da bei der Verarbeitung des ersten Blocks noch kein Geheimtext-Block zur Verfügung steht, wird ein sogenannter «Initialisierungsvektor» verwendet.

![Verschlüsselung im CBC-Modus ©](./images/cbc_encryption.svg)

Ändert man 1 Bit im IV, führt dies zu Änderungen im gesamten Geheimtext. Ändert man 1 Bit im Klartext, so ändern sich auch sämtliche darauffolgenden Blöcke im Geheimtext.

Ein Nachteil des CBC-Modus ist allerdings, dass die Verschlüsselung nicht parallelisiert werden kann, da das Resultat des vorherigen Blocks für die Verschlüsselung des aktuellen Blocks benötigt wird. D.h. ein bestimmter Klartext-Block kann erst verschlüsselt werden, wenn sämtliche vorherigen Blöcke bereits verschlüsselt sind.

Bei der Entschlüsselung sieht es anders aus. Da sofort sämtliche Geheimtextblöcke vorliegen, kann die Entschlüsselung problemlos parallelisiert werden, wie du in der folgenden Abbildung nachvollziehen kannst:

![Entschlüsselung im CBC-Modus ©](./images/cbc_decryption.svg)

::: info Bemerkenswert
Auf den ersten Blick erstaunlich ist die Tatsache, dass die Entschlüsselung mit falschem IV nur dazu führt, dass der erste Klartext-Block unleserlich ist, während die restlichen Blöcke korrekt entschlüsselt werden.
:::

::: exercise Aufgabe CBC-Modus
1. Verschlüssle nochmals denselben Text, diesmal allerdings im CBC-Modus:

   Klartext: `EINE SEHR KLEINE SENSATION`

   Schlüssel `ZUSE`

2. Achte wiederum auf die Blöcke, die im Klartext übereinstimmen. Was passiert jetzt?
3. Wähle verschiedene Initialisierungsvektoren. Wie unterscheiden sich die Resultate?
:::

::: exercise Aufgabe Initialisierungsvektor
Überlege dir zusammen mit deinem Pultnachbarn/deiner Pultnachbarin, mit welcher einfachen Massnahme man darauf verzichten könnte, neben dem Schlüssel auch den Initialisierungsvektor mit dem/r Kommunikationspartner/in abzusprechen.

***

Fügt man vor der eigentlichen Nachricht am Anfang einen zufälligen, sinnlosen Text in der Länge der Blocklänge hinzu, so kann der/die Kommunikationspartner/in den Geheimtext entschlüsseln, ohne den IV kennen zu müssen.

Dies bedeutet, dass der IV völlig zufällig gewählt werden kann und nicht übertragen werden muss, was zu einem zusätzlichen Sicherheitsgewinn führt.
:::

::: exercise Aufgabe Angriff auf die XOR-Blockchiffre
Setzt euch in Dreiergruppen zusammen und diskutiert folgende Fragen:

1. Welche Faktoren beeinflussen die Sicherheit unserer XOR-Blockchiffre?
2. Stellt zu jedem Faktor eine Aussage auf, die aufzeigt, wie eine möglichst hohe Sicherheit erreicht werden kann.
3. Wie sicher ist die Verwendung von XOR (im Kasten «block cipher encryption/decryption» in den Abbildungen oben) als Blockchiffren-Verfahren?

***

1. Es sind insbesondere die folgenden Faktoren:
   - die Länge des Schlüssels resp. die Blocklänge,
   - die Art und Weise, wie dieser Schlüssel gewählt wird (Benutzer wählt Passwort, zufällige binäre Zahlenfolge, ...)
2. Folgende Aussagen sind wichtig:
   - Der Schüssel/die Blocklänge sollte so gewählt werden, dass man den Schlüsselraum nicht in sinnvoller Zeit durchprobieren kann (also mind. 128 bit, besser 256 oder 512).
   - Der Schlüssel sollte von einem Computer zufällig gewählt werden, denn basiert der Schlüssel auf einem Passwort, können Angreifer versuchen, das Passwort anzugreifen anstatt den ganzen Schlüsselraum abzusuchen.
3. Die XOR-Verschlüsselung ist unsicher! Für die folgende Überlegung denken wir an den ersten Block, die Aussage gilt aber auch für sämtliche folgenden Blöcke. Jedes einzelne Bit aus dem Geheimtext wird nur vom entsprechenden Bit des Klartexts und Schlüssels beeinflusst, es findet keine «Vermischung» innerhalb des Blocks statt. Ändert ein Bit des Klartexts, ändert im entsprechenden Block des Geheimtexts auch nur genau das entsprechende Bit. Viel besser wäre es, wenn jede kleine Änderung eine grosse Auswirkung (durch Vermischung) hätte. Dies wird von modernen Blockchiffren selbstverständlich gemacht.
:::

::: info XOR-Blockchiffre
Selbstverständlich ist XOR als Verschlüsselungsfunktion aus kryptologischer Sicht nicht ideal – dafür aber für uns gut verständlich, überschaubar und einfach anzuwenden.

In der Realität ist es besonders wichtig, dass die Verschlüsselungsfunktion sämtliche Bits des aktuellen Blocks stark «vermischt». Damit ist gemeint, dass eine winzige Änderung an einer bestimmten Stelle im Klartextblock nicht nur zu einer kleinen Änderung an derselben Stelle im Geheimtextblock führt (wie dies bei XOR wegen der bitweisen Verarbeitung der Fall), sondern dass durch die kleine Änderung viele Bits überall im Block verändert werden.

Aus diesem Grund arbeiten aktuelle Verschlüsselungsfunktionen von Blockchiffren in mehreren Runden.
:::
