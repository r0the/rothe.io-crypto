# Codierungen
---

## Pentacode

Der Pentacode ist keine offizielle Codierung. Sie wurde von uns entwickelt, um erstens ein kurzes Alphabet ohne unnötige Zeichen zu verwenden und zweitens einen möglichst kurzen Binärcode zu erhalten.

<v-pentacode/>


## Schwarzweiss-Bild

<VuePixelEditor height=5 width=5 cell-size=20/>
