# Blockchiffre
---

Im vorherigen Beispiel war der Schlüssel gleich lang wie der Klartext. Da dies in der Realität schwierig zu bewerkstelligen ist, wurden **Blockchiffren** erfunden. Sie heissen so, weil der Text nicht mehr als ganzes, sondern in **Blöcken** verschlüsselt wird.

::: info Aktuelle Verschlüsselungsverfahren
Aktuelle Verschlüsselungsverfahren sind so komplex, dass wir sie unmöglich verstehen können. Daher werden wir viele der nachfolgend erwähnten Verfahren nur anhand von Analogien und nicht auf mathematischer Ebene kennenlernen.

Beim Thema Blockchiffren wollen wir aber kurz eintauchen und uns anhand einer einfachen Verschlüsselung (XOR) anschauen, wie moderne Verschlüsselung funktioniert.
:::

## Blocklänge

Da der Klartext dreimal so lange ist wie der Schlüssel, muss der Text in drei Teile aufgeteilt werden, welche wir separat verschlüsseln. Die Blocklänge entspricht also der Schlüssellänge:

::: columns 2
| Klartext   |                                                                                             |
|:---------- |:------------------------------------------------------------------------------------------- |
| Buchstaben | `GEHEIMESTREFFEN`                                                                           |
| Pentacode  | `00111 00101 01000 00101 01001 01101 00101 10011 10100 10010 00101 00110 00110 00101 01110` |
***
| Schlüssel  |                                 |
|:---------- |:------------------------------- |
| Buchstaben | `PENTA`                         |
| Pentacode  | `10000 00101 01110 10100 00001` |
:::

## Verschlüsselung in Blöcken

Im folgenden Beispiel wird der oben bereits erwähnte Klartext mit dem Schlüssel bitweise mit XOR verschlüsselt:

| Verschlüsselung    | Text    | Pentacode                       |
|:------------------ |:------- | ------------------------------- |
| Klartext Block 1   | `GEHEI` | `00111 00101 01000 00101 01001` |
| Schlüssel          | `PENTA` | `10000 00101 01110 10100 00001` |
| Geheimtext Block 1 | `W FQH` | `10111 00000 00110 10001 01000` |

| Verschlüsselung    | Text    | Pentacode                       |
|:------------------ |:------- | ------------------------------- |
| Klartext Block 2   | `MESTR` | `01101 00101 10011 10100 10010` |
| Schlüssel          | `PENTA` | `10000 00101 01110 10100 00001` |
| Geheimtext Block 2 | `. . S` | `11101 00000 11101 00000 10011` |

| Verschlüsselung    | Text    | Pentacode                       |
|:------------------ |:------- | ------------------------------- |
| Klartext Block 3   | `EFFEN` | `00101 00110 00110 00101 01110` |
| Schlüssel          | `PENTA` | `10000 00101 01110 10100 00001` |
| Geheimtext Block 3 | `UCHQO` | `10101 00011 01000 10001 01111` |

::: exercise Aufgabe XOR-Blockchiffre
Verschlüssle den folgenden Text mit der XOR-Blockchiffre:

**Text**: `PAKET ZUGESTELLT`

**Schlüssel**: `BETA`

1. Codiere zuerst den Text mit Pentacode (nicht manuell, sondern mit Hilfe des interaktiven Tools).
2. Codiere anschliessend den Schlüssel mit Pentacode.
3. Verschlüssle den binär dargestellten Text anschliessend mit dem binär dargestellten Schlüssel mittels XOR-Blockchiffre.
4. Decodiere ihn wieder mit Pentacode.

***

1. `10000 00001 01011 00101 10100 00000 11010 10101 00111 00101 10011 10100 00101 01100 01100 10100`
2. `00010 00101 10100 00001`
3. `10010 00100 11111 00100 10110 00101 01110 10100 00101 00000 00111 10101 00111 01001 11000 10101`
4. `RD@DVENTE GUGIXU`
:::
