# Asymmetrie
---

Wir haben in den beiden vorherigen Kapiteln gesehen, dass es Vorgänge gibt, die in die eine Richtung einfach durchzuführen sind, in die entgegengesetzte Richtung allerdings sehr aufwändig oder gar unmöglich:

| einfacher Vorgang               | aufwändiger/schwieriger Vorgang    |
|:------------------------------- |:---------------------------------- |
| offenes Bügelschloss schliessen | Bügelschloss ohne Schlüssel öffnen |
| Farben mischen                  | Farben trennen                     |

Diese Beispiele zeigen deutlich, worauf die asymmetrische Verschlüsselung basiert:

::: info Asymmetrische Verschlüsselung
Die asymmetrische Verschlüsselung basiert auf Aufgaben, die in eine Richtung einfach auszuführen sind, während man eine geheime Information braucht, um den Vorgang rückgängig zu machen.

Verfügt jemand nicht über diese geheime Information, ist die Umkehrung des Vorgangs nicht in sinnvoller Zeit zu bewältigen.
:::

## Ein mathematisches Problem dieser Art

Auch in der Mathematik gibt es Operationen, die einfach und schnell auszuführen sind. Die Umkehrung jedoch ist selbst für einen Computer aufwändig und kann Jahre dauern.

Ein Beispiel dafür ist das Multiplizieren zweier (Prim-)Zahlen. Jeder Computer kann pro Sekunde mehrere Milliarden Multiplikationen ausführen. Ein Produkt zweier Primzahlen in die beiden Faktoren zu zerlegen, ist jedoch ungleich aufwändiger – insbesondere wenn die Zahlen mehrere hundert Stellen lang sind.

::: exercise Aufgabe Multiplizieren ↔ Faktorisieren
1. Berechne `41 * 83` auf Papier. Überlege dir dabei, wie du vorgehst.

2. Schaffst du es, die Zahl `3397` in ihre **zwei** Primfaktoren zu zerlegen? Und `1117`? Wie könnte man dabei vorgehen?
***
1. `3403`
2. `43 * 79 = 3397`

   `1117` ist eine Primzahl und kann nicht in Primfaktoren zerlegt werden.
:::

::: exercise Aufgabe Aufwand für den Computer
1. Wie schnell kann der Computer multiplizieren und faktorisieren?

   Probiere mit dem nachfolgenden Tool, wie schnell dein Computer die Aufgaben erledigt.

2. Stell dir vor, dass der Computer für kryptographische Anwendungen mit Primzahlen rechnet, die mehrere hundert Stellen lang sind!
:::

<v-factorise/>
