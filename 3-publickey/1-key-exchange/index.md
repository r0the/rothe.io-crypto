# Schlüsselaustausch
---

Alice und Bob möchten sich geheime Unterlagen senden. Doch wie können sie den Schlüssel austauschen?

![Schlüsselaustausch](./images/key-exchange.svg)

Vielleicht hilft es, wenn wir uns in Erinnerung rufen, dass im Handel mehrere Schlösser mit unterschiedlichen Schlüsseln zu kaufen sind:

![Schlüsselaustausch](./images/key-exchange-2.svg)

::: exercise Schlüsselaustausch
Findet eine Möglichkeit, den Inhalt der Truhe sicher zu übertragen?

Versetzt euch dabei auch in die Rollen der anderen Krypto-Charaktere!
***
1. Variante 1

   Alice verschliesst die Truhe mit dem eigenen Schloss, um ungewollten Zugriff zu unterbinden. So verschlossen schickt sie die Truhe an Bob. Auch er kann sie nicht öffnen. Daher fügt er noch sein Schloss hinzu und sendet die Truhe zurück. Nun kann Alice ihr Schloss entfernen und die Truhe ein letztes Mal schicken.

   ![](./images/asymm-encryption-2.svg)

   Dass es sich um Alices Schloss handelt, kann Bob glauben, da die Absenderin die Informationen ja sicher nicht offen transportieren würde.

   **Problem**: Wie weiss Alice, dass es sich um Bobs Schloss handelt? Jemand unterwegs könnte das Schloss austauschen.

2. Variante 2

   Bob könnte Alice das eigene Schloss senden, mit dem Alice anschliessend die Truhe verschliesst.

   ![](./images/asymm-encryption-1.svg)

   **Problem**: Wie weiss Alice, dass es sich um Bobs Schloss handelt? Jemand unterwegs könnte das Schloss austauschen.

3. Variante 3

   Hier handelt es sich eigentlich um die erste, einfache Variante mit nur einer Sendung. Allerdings erhält Alice das Schloss nicht von Bob, sondern von einer vertrauenswürdigen Dritten Stelle (Trent). Trent hat eine ganze Sammlung von Schlössern – natürlich nicht nur von Bob, sondern von ganz vielen Leuten. So können alle, die Trent vertrauen, dort Schlösser abholen.

   ![Schlösser beim vertrauenswürdigen Trent beziehen](./images/asymm-encryption-3.svg)
:::
