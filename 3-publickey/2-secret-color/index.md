# Geheime Farbe
---

Alice und Bob arbeiten an einem neuen Kunstwerk, auf das die Öffentlichkeit gespannt wartet. Die beiden möchten dafür eine ganz besondere Farbe verwenden. Diese Farbe soll aber unbedingt bis zur Vernissage geheim bleiben. Alice und Bob wohnen weit auseinander und können sich nicht treffen, um die geheime Farbe gemeinsam herzustellen, sie können sich lediglich Farbkübel per Post zusenden.

## Alice und Bob

Sie haben eine Idee und gehen wie folgt vor:

1. Alice und Bob mischen sich je in einem Farbkübel eine persönliche, geheime Farbe, die sie **niemandem** mitteilen (*private Farbe* genannt).
2. Alice wählt nun zusätzlich eine Farbe, die nicht geheim gehalten wird. Sie füllt zwei grosse Farbkübel mit dieser Farbe, einen behält sie für sich selbst, den anderen schickt sie per Post an Bob (*gemeinsame Farbe* genannt).
3. Im nächsten Schritt mischen sich Alice und Bob je in einem leeren Farbkübel eine neue Farbe: Sie nehmen dazu genau dieselbe Menge der eigenen privaten Farbe und der gemeinsamen Farbe. Diese neue Farbe schicken sie sich wieder gegenseitig zu.
4. Im letzten Schritt nehmen sie zwei Einheiten der soeben erhaltenen Farbe und eine Einheit der privaten Farbe und erhalten die *gemeinsame private Farbe*, mit der sie die Teile des neuen Kunstwerks bemalen.

## Eve

Die neugierige Journalistin Eve möchte unbedingt wissen, was Alice und Bob aushecken, um noch vor der Vernissage einen exklusiven Zeitungsbericht zu veröffentlichen. Daher versucht sie, an die gemeinsame private Farbe zu gelangen. Sie überwacht die Post und füllt sich von jeder transportierten Farbe ein wenig in eigene Behälter ab.

## Ausprobieren[^1]

Werde selbst aktiv, bestimme die Farben für Alice und Bob und schaue dir die Ergebnisse an. In der Mitte siehst du die Farben, die ausgetauscht werden. Aussen werden die *privaten Farben* angezeigt.

<v-colour-diffie-hellman/>

## Fragen

::: exercise Geheime Farbe herausfinden
1. Wieso erhalten Alice und Bob schlussendlich dieselbe Farbe?
2. Überlege, wieso Eve aus den verschickten Farben die *geheime Farbe* nicht herstellen kann?
:::

::: exercise Digitale Farben
Würde dieses Verfahren auch mit digitalen Farben (also RGB-Werten) funktionieren?

Besprecht dies zu zweit und begründet!
:::

[^1]: Quelle der Avatare: [freepik](https://www.freepik.com/free-vector/office-worker-set_1539145.htm), erstellt von [macrovector](https://www.freepik.com/macrovector)
