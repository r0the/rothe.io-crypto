# Asymmetrische Verschlüsselung
---

Das Prinzip der asymmetrischen Verschlüsselung beruht also auf zwei verschiedenen Schlüsseln, dem sogenannten **Schlüsselpaar** (engl. *key pair*). Die beiden  Schlüssel eines Schlüsselpaars nennt man **öffentlichen** und **privaten Schlüssel** (engl. *public* und *private key*).

Da für den Verschlüsselungsvorgang **nicht** derselbe Schlüssel verwendet wird wie für den Entschlüsselungsvorgang, sprich man von einem asymmetrischen Verfahren.

::: columns 2
![Asymmetrische Ver- und Entschlüsselung mit verschiedenen Schlüsseln](./images/asymmetric-cryptosystem.svg)
***
![Generierung des Schlüsselpaares für die asymmetrische Verschlüsselung ](./images/key-pair-generation.svg)
:::

### Schlüsselpaar

Jede Person besitzt ein eigenes Schlüsselpaar, so auch Bob. Die beiden Schlüssel, die dieses Schlüsselpaar bilden, sind mathematisch verwandt. Der private Schlüssel lässt sich aus dem öffentlichen Schlüssel nicht in sinnvoller Zeit berechnen.

Bobs öffentlicher Schlüssel entspricht also dem Bügelschloss aus unserem ersten Beispiel. Bobs privater Schlüssel ist der Schlüssel, der zum Bügelschloss passt. Das Schloss kann (in geöffneter Form natürlich) bei einer vertrauenswürdigen Stelle (Trent) deponiert werden, wo es von Alice abgeholt werden kann. Den Schlüssel behält Bob stets für sich.

![Jede Person besitzt ein Schlüsselpaar](./images/key-pair.svg)

### Verschlüsselung

In der folgenden Abbildung sieht man auf der linken Seite, dass Alice bei der Verschlüsselung den **öffentlichen Schlüssel von Bob** (und nicht den eigenen) verwendet. Dieser öffentliche Schlüssel enthält keine geheime Information und ist für alle Leute zugänglich (z.B. via eine vertrauenswürdige Stelle), somit können alle eine Nachricht für Bob verschlüsseln.

![Asymmetrische Verschlüsselung](./images/asymm-encryption.svg)

### Entschlüsselung

In derselben Abbildung ist zu sehen, dass Bob bei der Entschlüsselung seinen **privaten Schlüssel** verwenden muss. Somit ist sichergestellt, dass nur Bob die Nachricht entschlüsseln kann.


::: info Achtung
Um Verwechslungen zu vermeiden, spricht man bei der symmetrischen Verschlüsselung vom **geheimen Schlüssel** (engl. *secret key*), während man bei asymmetrischen Verfahren vom **privaten Schlüssel** spricht und nicht vom *geheimen Schlüssel*, weil jede involvierte Person ein eigenes Schlüsselpaar und somit einen eigenen **privaten Schlüssel** besitzt.
:::

::: exercise Aufgabe asymmetrische Verschlüsselung
Überlege dir und zeichne, wie die Verschlüsselung funktioniert, wenn ein Dokument an verschiedene Leute versendet wird.

Welche Nachteile erkennst du?
***
![Asymmetrische Verschlüsselung für zwei Personen](./images/asymm-encryption-for-two.svg)

**Nachteile**

- Das Dokument muss separat für zwei Personen verschlüsselt werden.
- Jeder Person muss eine andere Nachricht geschickt werden.
:::
